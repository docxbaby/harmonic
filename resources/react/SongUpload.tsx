import * as React from 'react'
import { Input } from '@mui/material';
import { Button } from '@mui/material';
import Swal from 'sweetalert2';


export default function SongUpload() {
    
    let file:File;

    const handleFileChange = (event) => {
        file = event.target.files[0];
        if (file.size > 10000000 ){
            console.log('el archivo es demaciado grande')
        }
        console.log('Selected file:', file);
        // Do something with the selected file
    };

    const style = {
        backgroundColor: 'rgb(40,40,40)',
        zIndex: '0',
        borderRadius: '.5rem',
        color: 'rgb(230,230,255)',
        padding: '1rem',
        margin: '1rem',
        borderStyle: 'solid',
        borderWidth: '2px',
        borderColor: 'white'
    }

    function getCSRFToken() {
        const csrf = document.querySelector('meta[name="csrf-token"]');
        if (csrf)
            return csrf.getAttribute('content');
        else return null;
    }


    async function upLoadFile(){
        console.log('enviando el archivo');
        const csrfToken = getCSRFToken();
        const formData = new FormData();

        if (!file){
            throw new Error("Hay un problema con el archivo");
        }
        formData.append('file', file);

        try {
            
            if (csrfToken){
                const init:RequestInit = { method: 'POST', headers:{ 'X-CSRF-TOKEN':csrfToken}, body:formData }
                const response = await fetch('http://localhost:8000/api/songs', init);
                
                if (!response.ok)
                    throw new Error("Error al enviar el archivo");

                response.text().then((e)=>{
                    console.log('repuesta:', e)
                    const res = JSON.parse(e); 
                    if (res.status == 'success'){
                        console.log('se guardo el archivo');
                    }
                    
                    if (res.status === 'error'){
                        Swal.fire( { title:'Error', text:res.message } )
                    }
                }).catch((e)=>{console.log(e)})
                
            }
        }catch(e){
            console.log('error al solicitar guardar la canción');
            console.log(e)
        }
    }

    return (<div style={style}>
        <h2 style={{ fontFamily: 'system-ui', display: 'grid', placeItems: 'center' }}>Carga una nueva canción</h2>
        <div style={{backgroundColor:'rgb(50,50,50)', padding:'.5rem'}}>
            <Input
                type="file"
                style={{ borderRadius: '.5rem', backgroundColor: 'rgb(0,100,0)', padding: '.5rem' }}
                onChange={handleFileChange}
                inputProps={{ 'aria-label': 'Elije el archivo' }}
            />
            <div style={{ display: 'grid', placeItems: 'center' }}>
                <Button onClick={upLoadFile} variant='outlined' style={{ display: 'block', margin: '1rem' }}> Guardar</Button>
            </div>
        </div>
    </div>)
}