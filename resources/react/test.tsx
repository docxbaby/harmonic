import React from 'react';

function Test() {
    return (
        <audio
            src={"http://localhost:8000/api/song/" + '9_Project.mp3'}
            style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            controls
            onPlay={() => { console.log('play') }}
            onSeeked={() => { console.log('seeked') }}
            onSeeking={() => { console.log('seeking') }}
            onTimeUpdate={()=>{console.log('timeupdating')}}
            autoPlay={false}
        ></audio>
    )
}

export default Test; 