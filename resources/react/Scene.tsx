import React from 'react';
import { TextField,Button } from '@mui/material';
import AppBar from './appBar';


export default function Scene() {
    let style = { padding: '1rem', backgroundColor: 'rgb(100,100,100)', margin: '1rem' };
    
    let textSearch = ''; 

    function onChangeSearch(event){
        console.log(event.target.value);
        textSearch = event.target.value;
    }

    return (
        <div>
            <AppBar />
            <div style={{border:'1px solid rgb(60,60,80)', padding:'1rem', backgroundColor:'rgb(50,50,80)'}}>
                <label htmlFor="search" style={{color:'white', fontFamily:'monspace', fontSize:'large', backgroundColor:'rgb(0,0,0)'}}>Buscar</label>
                <TextField id='search'  onChange={onChangeSearch} disabled={false} ></TextField>
                <Button style={{display:'block', margin:'1rem'}} variant='contained'>Buscar</Button>
            </div>
        </div>)
}