import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Navigate } from 'react-router-dom';


export default function PositionedMenu() {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [goToDashboard, setGoToDashboard] = React.useState(false);

    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleSongs = () => {
        handleClose();
        setGoToDashboard(true);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = ()=> {
        fetch('http://localhost:8000/api/logout').then((e)=>{
        window.location.reload();
        }).catch((e)=>{
            console.log('error', e)
        })

        //window.location.reload()
        handleClose();
    }

    return (
        <div>
            <Button
                id="demo-positioned-button"
                aria-controls={open ? 'demo-positioned-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            >
                Menú
            </Button>
            <Menu
                id="demo-positioned-menu"
                aria-labelledby="demo-positioned-button"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <MenuItem onClick={handleSongs}>Canciones</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
                {goToDashboard && <Navigate to="/dashboard" replace={true} />}
            </Menu>
        </div>
    )
}
