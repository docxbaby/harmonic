import ReactDOM from 'react-dom/client';
import React from 'react';
import Main from "./Main";
import './index.css'
import Login from './login';
import Dashboard from './Dashboard';
import { BrowserRouter, Routes, Route, Link, Navigate } from 'react-router-dom';
import Profile from './Profile';
import Scene from './Scene';
import Song from './Song';
import Test from './test';


fetch('http://localhost:8000/logged').then(e => {
    console.log(e);
    e.text().then((res) => {
        const jsonres = JSON.parse(res);
        if (jsonres.status === 'success' && jsonres.logged === true) {
           console.log('renderizando main')
           ReactDOM.createRoot(document.getElementById('app')).render(
               <Main/>
            );
        } else {
            console.log('renderizando login')
            ReactDOM.createRoot(document.getElementById('app')).render(
                <div>
                    <div>
                        <img style={{
                            backgroundImage: `url('images/Harmonic_blue.png')`,
                            backgroundRepeat: 'repeat',
                            width: '100vw',
                            height: '100vh',
                            position: 'fixed',
                            zIndex: '-10'
                        }} />
                        <img style={{
                            backgroundImage: `url('images/Harmonic_pink.png')`,
                            backgroundRepeat: 'repeat',
                            width: '100vw',
                            height: '100vh',
                            position: 'fixed',
                            zIndex: '-9'
                        }} />
                    </div>
                    <Login />
                    <div style={{ display: 'flex', justifyContent: 'center', bottom: 0, position: 'absolute', backgroundColor: 'rgb(120,70,130)', color: 'white', width: '100%' }}>Harmonic 2027 Autor: Juan Pablo Hernández Guzman</div>
                </div >
            );
        }
    })
})
