import React, { useState } from 'react'
import { Button } from '@mui/material'
import { Navigate } from 'react-router-dom'
import { Any } from 'react-spring'

const style = {
    backgroundColor: 'rgb(40,40,40)',
    zIndex: '0',
    borderRadius: '.5rem',
    color: 'rgb(230,230,255)',
    padding: '1rem',
    margin: '1rem',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'rgb(50,50,50)'
}



async function getSongs(hook) {

    const init: RequestInit = {
        method: 'GET'
    }

    fetch('http://localhost:8000/api/songs_v2', init)
        .then((e) => {
            e.text().then((e) => {
                const jsonRes = JSON.parse(e);
                console.log(jsonRes)
                hook(jsonRes);
            })

        }).catch((e) => {
            console.log('error al solicitar las canciones');
            console.log(e)
        })
}

export default function MySongs({ setSongFunction }) {

    //hook para renderizar dinamicamente
    const [songsRequest, setSongs] = useState<any[]>([]);


    //si no se han solicitado las canciones
    if (songsRequest.length == 0) {
        getSongs(setSongs); //las solicitamos
    }

    //hook para ir a la edicion de archivo
    let [goToSong, setGoToSong] = React.useState(false);

    //manejador del evento editar
    function handleEditar(song) {
        setGoToSong(true);
        setSongFunction(song)
    }

    return (
        <div style={style}>
            <h2 style={{ fontFamily: 'system-ui', display: 'grid', placeItems: 'center' }}>Mis canciones</h2>

            {/* por cada cancion*/}
            {songsRequest.map((song, index) =>{ 
                
                if (song.metadata.metadata)
                return (
                <div style={{ display: 'block', 
                    margin: '.5rem', 
                    backgroundColor: 'rgb(30,30,30)', 
                    padding: '1rem' }} key={index}>
                    <div style={{ margin: '.4rem', fontFamily: 'monospace', display: 'grid', gridTemplateColumns: '50% 50%' }}>
                        <span style={{ display: 'block' }}> Archivo: {song.file} </span>
                        <span style={{ display: 'block' }}> Nombre: {song.metadata.metadata.title} </span>
                        <span style={{ display: 'block' }}> Género: {song.metadata.metadata.genre} </span>
                        <span style={{ display: 'block' }}> Artista: {song.metadata.metadata.artist} </span>
                        <Button onClick={() => { handleEditar(song) }} style={{ textTransform: 'none', float: 'right' }}>Editar</Button>
                    </div>
                    <audio
                        src={'http://localhost:8000/api/song/' + song.file}
                        controls
                        //onTimeUpdate={handleTimeUpdate}
                        autoPlay={false}
                    />
                    {goToSong && <Navigate to='/song' />}
                </div> )
            else {
                return (
                <div key={index}>
                    Error en los datos de {song.file}
                    <Button onClick={() => { handleEditar(song) }} style={{ textTransform: 'none', float: 'right' }}>Editar</Button>
                    {goToSong && <Navigate to='/song' />}

                </div>)
            }    
            }
                )
            }

        </div>
    )
}