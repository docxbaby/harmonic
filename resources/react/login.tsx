import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import TextField from '@mui/material/TextField';
import NewAccountForm from './newAccountForm';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const color = "rgb(100, 100, 255)"
const textColor = "rgb(150, 100, 255)"

const darkGray = '#43556b'
const darkerGray = 'rgb(32 39 47)'
const loginBorderColor = 'rgb(50,50,150)'
const blueMonster = 'rgb(100, 150, 255)';
const blueMonsterDark = 'rgb(80, 130, 235)';
const focusedColors = {
    '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
        borderColor: blueMonster,
    },
    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
        borderColor: blueMonsterDark,
    }
};

/**
 * 
 * @returns laravel token
 */
function getCSRFToken() {
    const csrf = document.querySelector('meta[name="csrf-token"]');
    if (csrf)
        return csrf.getAttribute('content');
    else return null;
}


/**
 * Login component
 * @returns JSX
 */
function Login() {
    /**
     * hooks
     */
    const [navigateToDashboard, setNavigateToDashboard] = useState(false);
    const [value, setValue] = React.useState('1');

    
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    async function sendDataForLogin(){
        if (userName.length == 0){
            console.log('agrega un nombre de usuario')
            return; 
        }
        
        if (password.length == 0){
            console.log('agrega una contraseña')
            return; 
        }

        try {
            const csrf = getCSRFToken();
            const formData = { 'username': userName, 'password': password }
            const headers = new Headers();

            if(csrf){
                headers.append('Content-Type', 'application/json');
                headers.append('X-CSRF-TOKEN', csrf);

                const response = await fetch('http://localhost:8000/api/login', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json', 
                        'X-CSRF-TOKEN': csrf,
                    },
                    body:JSON.stringify(formData)
                })
    
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                console.log('respondio al buscar el usuario')
                
                response.text().then((e)=>{
                    console.log(e)
                    const respObj = JSON.parse(e);
                    if (respObj.status == 'success'){
                        //setNavigateToDashboard(true);
                        window.location.reload();
                    }
                    else{
                        Swal.fire({title:'Usuario desconocido', text:'verifica las credenciales', icon:'error'})
                        console.log(respObj)
                    }

                }).catch((e)=>{
                    console.log(e)
                })

                
            }

            
        } catch (error) {
            
        }
    }

    let userName = '';
    function onUserChange(event){
        userName = event.target.value; 
    }

    let password = '';
    function onPassChange(event){
        password = event.target.value; 
    }

    
    return (
        <div style={{
            display: 'grid',
            placeItems: 'center',
            height: '100vh',
        }}>
            {navigateToDashboard && <Navigate to="/dashboard" replace={true} />}
            <div
                style={{
                    borderStyle: 'solid',
                    borderWidth: '2px',
                    borderColor: 'rgb(100,150,255)',
                    zIndex: '0',
                    borderRadius: '.5rem',
                    padding: '1rem',
                    backgroundColor: darkerGray,
                    color: 'rgb(100,150,255)',

                }}
            >
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChange} aria-label="lab API tabs example">
                            <Tab label="Ingresar" value="1" />
                            <Tab label="Nueva cuenta" value="2" />
                        </TabList>
                    </Box>
                    <TabPanel value="1">
                        <span style={{ fontFamily: 'system-ui', fontSize: 'larger', margin: '.5rem' }}>Ingresa a tu cuenta</span>
                        <TextField
                            id="textFieldUser"
                            label="Usuario"
                            variant="outlined"
                            style={{ display: 'block', margin: '1rem' }}
                            inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                            InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                            sx={focusedColors}
                            onChange={onUserChange}
                        />
                        <TextField
                            id="textFieldPass"
                            label="Contraseña"
                            type='password'
                            variant="outlined"
                            style={{ display: 'block', margin: '1rem' }}
                            inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                            InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                            sx={focusedColors}
                            onChange={onPassChange}
                        />
                        <div style={{ display: 'grid', placeItems: 'center' }}>
                            <Button onClick={sendDataForLogin} style={{ color: blueMonster, borderColor: blueMonster, textTransform: 'none' }} variant='outlined'>Ingresar</Button></div>
                    </TabPanel>
                    <NewAccountForm/>
                </TabContext>
            </div>
        </div>
    );
}

export default Login;