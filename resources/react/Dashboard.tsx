import React from 'react';
import ButtonAppBar from './appBar';
import SongUpload from './SongUpload';
import MySongs from './MySongs';
import { Navigate } from 'react-router-dom';


export default function Dashboard ({setEditingSong}){
    let [testvar, setTest] = React.useState(false);

    return (
        <div>
            <ButtonAppBar></ButtonAppBar>
            <div style={{display:'grid', placeItems:'center'}}>
                <SongUpload></SongUpload>
                <MySongs setSongFunction= {setEditingSong}></MySongs>
                <button onClick={()=>{setTest(true)}}>test</button>
                {testvar && <Navigate to='/test' />}
            </div>
        </div>
    )
}