import * as React from 'react'
import TabPanel from '@mui/lab/TabPanel';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import SimpleAlert from './Alert';
import { useState } from 'react';



export default function NewAccountForm() {
    const [showAlert, setShowAlert] = useState(false);


    const blueMonster = 'rgb(100, 150, 255)';
    const blueMonsterDark = 'rgb(80, 130, 235)';
    const focusedColors = {
        '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
            borderColor: blueMonster,
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: blueMonsterDark,
        }
    };

    let passConf = '';
    function handleChageConfirm(event) {
        passConf = event.target.value;
    }

    let pass = ''
    function handleInputPass(event) {
        pass = event.target.value;
    }

    function equalPasswords() {
        return pass == passConf;
    }


    //check if pass or confirm is empty
    function isEmptyPassword() {
        return pass.length == 0 && passConf.length == 0
    }

    let userName = '';
    function onUserInputChange(event) {
        userName = event.target.value;
    }

    let email = '';
    function onEmailInputChange(event) {
        email = event.target.value;
    }

    function empty_user_and_email() {
        return email.length == 0 || userName.length == 0;
    }

    function lengthTooShort(str, length) {
        return str.length < length
    }

    function checkEmailFormat() {
        if (email.match(/@.*\./))
            return true;
        else return false;
    }

    function getCSRFToken() {
        const csrf = document.querySelector('meta[name="csrf-token"]');
        if (csrf)
            return csrf.getAttribute('content');
        else return null;
    }

    function onclickRegister() {


        if (lengthTooShort(userName, 6)) {
            console.log('nombre de usuario demaciado corto')
            return;
        }
        if (!checkEmailFormat()) {
            console.log('el email no tiene un formato adecuado')
            return
        }

        if (lengthTooShort(pass, 8)) {
            console.log('contraseña demaciado corta')
            return;
        }

        if (empty_user_and_email()) {
            console.log('faltan datos por llenar');
            return;
        }

        if (isEmptyPassword()) {
            console.log('coloca una contraseña')
            return;
        }

        if (equalPasswords()) {
            console.log('los password son iguales')
            sendUserData()


        }
        else {
            console.log('no son iguales')
            return;

        }



    }

    async function sendUserData() {
        const csrf = getCSRFToken()
        if (csrf) {
            try {
                console.log(userName)
                console.log(email)
                console.log(pass)
                const formData = {'username': userName,
                'email': email ,
                'password': pass } 

                console.log('enviando')
                console.log(formData)

                const response = await fetch('http://localhost:8000/api/save-user', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json', 
                        'X-CSRF-TOKEN': csrf,

                    },
                    body:JSON.stringify(formData)
                })

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                setShowAlert(true)

                const responseData = await response.text();
                console.log('Response:', responseData);
            } catch (error) {
                console.error('Error:', error);
            }
        }
    }

    return (
    
        <TabPanel value="2">
            <span style={{ fontFamily: 'system-ui', fontSize: 'larger', margin: '.5rem' }}>Ingresa a tu cuenta</span>
            <TextField
                id="textFieldUserS"
                label="Usuario"
                variant="outlined"
                style={{ display: 'block', margin: '1rem' }}
                inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                sx={focusedColors}
                onChange={onUserInputChange}
            />
            <TextField
                id="emailFieldUserS"
                label="email"
                variant="outlined"
                type='email'
                style={{ display: 'block', margin: '1rem' }}
                inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                sx={focusedColors}
                onChange={onEmailInputChange}
            />
            <TextField
                id="textFieldPassS"
                label="Contraseña"
                type='password'
                variant="outlined"
                style={{ display: 'block', margin: '1rem' }}
                inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                sx={focusedColors}
                onChange={handleInputPass}
            />
            <TextField
                id="confirmFieldPassS"
                label="Confirma"
                type='password'
                variant="outlined"
                style={{ display: 'block', margin: '1rem' }}
                inputProps={{ style: { color: 'rgb(100, 200, 200)' } }}
                InputLabelProps={{ style: { color: 'rgb(190, 160, 255)' } }}
                sx={focusedColors}
                onChange={handleChageConfirm}
            />
            <div style={{ display: 'grid', placeItems: 'center' }}><Button onClick={onclickRegister} style={{ color: blueMonster, borderColor: blueMonster, textTransform: 'none' }} variant='outlined'>Crear</Button></div>
            {showAlert && <SimpleAlert message="Usuario agregado" show={showAlert}/>  }
        </TabPanel>)
}