import React from "react";
import { Input, TextField, Button, Checkbox } from "@mui/material";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import AppBar from "./appBar";
import { Navigate } from "react-router-dom";
import Artist from './Artist';
import { styled } from '@mui/material/styles';
import Tooltip from '@mui/material/Tooltip';
import Stack from '@mui/material/Stack';
import { DemoContainer, DemoItem } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';


export default function Profile() {

    let [updateImage, setUpdateImage] = React.useState(1);
    let [userData, setUserData] = React.useState<any>(null);

    const ProSpan = styled('span')({
        display: 'inline-block',
        height: '1em',
        width: '1em',
        verticalAlign: 'middle',
        marginLeft: '0.3em',
        marginBottom: '0.08em',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundImage: 'url(https://mui.com/static/x/pro.svg)',
    });

    function Label({
        componentName,
        valueType,
        isProOnly,
    }: {
        componentName: string;
        valueType: string;
        isProOnly?: boolean;
    }) {
        const content = (
            <span>
                <strong>{componentName}</strong>
            </span>
        );

        if (isProOnly) {
            return (
                <Stack direction="row" spacing={0.5} component="span">
                    <Tooltip title="Included on Pro package">
                        <a
                            href="https://mui.com/x/introduction/licensing/#pro-plan"
                            aria-label="Included on Pro package"
                        >
                            <ProSpan />
                        </a>
                    </Tooltip>
                    {content}
                </Stack>
            );
        }

        return content;
    }


    //=================================> DATOS DEL USUARIO <============================================

    function getUserData() {
        if (userData === null)
            fetch('http://localhost:8000/api/userData').then((response) => {
                response.text().then((textResp) => {
                    const jres = JSON.parse(textResp);
                    if (jres.status === 'success') {
                        setUserData(jres.userData)
                        console.log(jres.userData);
                    }
                })
            })
    }

    getUserData();

    function getCSRFToken() {
        const csrf = document.querySelector('meta[name="csrf-token"]');
        if (csrf)
            return csrf.getAttribute('content');
        else return null;
    }


    //=============================================> HANDLE DE LA IMAGEN DE PERFIL <============================

    function handle(event) {
        const file = event.target.files[0];
        const form = new FormData();
        form.append('file', file);
        console.log('subiendo el archvio');

        const csrf = getCSRFToken();

        if (csrf) {
            fetch('http://localhost:8000/api/profile/image', {
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': csrf },
                body: form
            }).then((r) => {
                r.text().then((r) => { console.log(r) })
                setUpdateImage(Math.random());
            }).catch((e) => {
                console.log('no se pudo agregar la imagen')
            })
        }
    }

    function getImage() {
        const csrf = getCSRFToken();

        if (csrf) {

            fetch('http://localhost:8000/api/profile/image', {
                method: 'GET',
                headers: { 'X-CSRF-TOKEN': csrf },
            }).then((r) => {
                console.log('el servidor encontro la imagen', r);

            }).catch((e) => {
                console.log('no se pudo agregar la imagen')
            })
        }
    }

   

    let artist = new Artist();

    const container = {
        borderStyle: 'solid',
        borderColor: 'rgb(20,20,30)',
        display: 'grid',
        placeItems: 'center',
        padding: '1rem',
        borderWidth: '1px',
        borderRadius: '.5rem',
        backgroundColor: 'rgb(50,80,105)',
        zIndex: '0'
    }

    

    /**
     * cuando seleccionas una imagen de portada
     */
    function handleCoverageImage(event) {
        console.log('subiendo portada');
        const coverImage = event.target.files[0];
        const csrf = getCSRFToken();
        const form = new FormData();
        form.append('coverage', coverImage);
        if (csrf) {
            fetch('http://localhost:8000/api/profile/coverage', {
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': csrf },
                body: form
            }).then((r) => {
                console.log(r)
                r.text().then((r) => { console.log(r) })
                getCoverage();
            }).catch((e) => {
                console.log('no se pudo agregar la imagen')
            })
        }
    }

    let [imageCoverage, setImageCoverage] = React.useState<any>(null);

    function getCoverage() {
        fetch('http://localhost:8000/api/profile/coverage', { method: 'GET' }).then((raw) => {
            raw.blob().then((e) => {
                const url = URL.createObjectURL(e);
                setImageCoverage(url);
            }).catch((e) => {
                console.log('no pudo ser convertido ablob');
            })
        });
    }

    if (!imageCoverage)
        getCoverage();

    

    //=========================================================> ENVIO DE LOS DATOS DEL FORMULARIO <==============================

    function sendData() {
        const token = getCSRFToken();
        const formData = new FormData();

        formData.append('fundationDate', date || '' );
        formData.append('artistName', artistName);
        formData.append('genre', genre);
        formData.append('biography', bio);

        if (token) {
            fetch('http://localhost:8000/api/profile/saveProfile', { method: 'POST', headers: { 'X-CSRF-TOKEN': token }, body: formData }).then(res => {
                console.log(res);
                res.text().then((e) => {
                    const jres = JSON.parse(e);
                    console.log(jres)
                }).catch((error) => {
                    console.log('error al convertirlo en json');
                })
            }).catch((error) => {

                console.log('error al enviar los datos ', error);
            })
        }

    }

    
    
    //===================================> INPUT DE LA BIOGRAFIA <==============================
    let [disableBio,setDisableBio]=React.useState(false);
    let [borderBio,setBorderBio]=React.useState('rgb(0,30,55)');
    let [bio,setBio]= React.useState('');
    
    function handleBiography(event) { setBio(event.target.value); }

    function inputBiography() {
        function toggle(){
            setDisableBio(!disableBio);

            if ( borderBio === 'rgb(0,30,55)' ) { 
                setBorderBio('rgb(70,130,125)') }
            else { 
                setBorderBio( 'rgb(0,30,55)' ) };
        }
        return <> <TextField
            disabled={!disableBio}
            onChange={handleBiography}
            style={{ marginTop: '1rem', width: '100%' }}
            multiline
            label={'Biografía'}
            placeholder={userData.biography || 'Vacío'}
            inputProps={{
                style: {
                    color: 'rgb(130,180,185)', //color de la letra
                    backgroundColor: 'rgb(30,60,85)'
                }
            }}
            InputLabelProps={{
                style: { color: 'rgb(130,180,220)' }
            }}
            sx={{
                '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
                    borderColor: borderBio,
                },
                '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                    borderColor: 'rgb(100,130,155)',
                }, '& .css-dpjnhs-MuiInputBase-root-MuiOutlinedInput-root': {
                    backgroundColor: 'rgb(30,60,85)'
                }
            }}></TextField>
            <Checkbox onClick={toggle}/>
        </>
    }


    //==========================================> INPUT DE LA FECHA DE FUNDACION <===========================================================

    let [date,setDate]=React.useState('');
    function handleDate(event) {
        let date = new Date(event.$d);
        let formatedDate = date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, '0') + '-' + String(date.getDay()).padStart(2, '0');
        console.log(formatedDate)
        setDate( formatedDate );
    }
    
    function inputDate() {
        return <>
            <LocalizationProvider dateAdapter={AdapterDayjs} >
                <DemoContainer
                    components={[
                        'DatePicker',
                        'TimePicker',
                        'DateTimePicker',
                        'DateRangePicker']}>
                    <DemoItem label={<Label componentName="Fecha de fundación" valueType="date" />}>
                        <DatePicker
                            sx={{
                                backgroundColor: 'rgb(30,60,85)',
                                '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                                    borderColor: 'rgb(100,130,155)'
                                }
                            }}
                            onChange={handleDate} />
                    </DemoItem>
                </DemoContainer>
            </LocalizationProvider>
            <Checkbox />
        </>
    }

    //=============================================> INPUT DE LA IMAGEN DE PERFIL <======================================
    function inputProfileImage() {

        return (
            <span className="profileContainer">
                <label
                    htmlFor="profileImage"
                    style={{
                        fontFamily: 'monospace',
                        color: 'rgb(130,180,185)',
                        fontSize: 'larger'
                    }}>
                    Imagen de perfil
                </label>
                <Input
                    id="coverageImage"
                    type="file"
                    onChange={handle}
                    className="profileInput" />
            </span>
        );
    }


    //=============================================> INPUT DE LA IMAGEN DE LA PORTADA <======================================
    function inputCoverageImage() {
        return <span className="profileInput2">
            <label htmlFor="profileImage"
                style={{
                    fontFamily: 'monospace',
                    color: 'rgb(130,180,185)',
                    fontSize: 'larger'
                }}>
                Imagen de portada
            </label>
            <Input
                id="profileImage"
                type="file"
                onChange={handleCoverageImage}
                className="profileImage" />
        </span>
    }

    
    //=============================================> INPUT DEL "GENERO" <======================================

    //para desactivar el input con el checkbox;
    let [disableGenre, setDisableGenre] = React.useState(false);
    
    //actualizar el stilo del border del input 
    let [colorBorderGenre, setColorBorderGenre]=React.useState('rgb(0,30,55)');

    let [genre,setGenre]=React.useState('');
    function handleGenre(event) {
        setGenre( event.target.value );
    }


    function inputGenre() {

        function toggle(){
            setDisableGenre(!disableGenre);

            if ( colorBorderGenre === 'rgb(0,30,55)' ) { 
                setColorBorderGenre('rgb(70,130,125)') }
            else {  
                setColorBorderGenre( 'rgb(0,30,55)' ) };
        }

        return <>
            <TextField
                disabled={!disableGenre}
                style={{ marginLeft: '.5rem ' }}
                onChange={handleGenre}
                label={'Género principal'}
                placeholder={userData.genre || 'Vacío'}
                inputProps={{
                    style: {
                        color: 'rgb(130,180,185)', //color de la letra
                        backgroundColor: 'rgb(30,60,85)'
                    }
                }}
                InputLabelProps={{
                    style: { color: 'rgb(130,180,220)' }
                }}
                sx={{
                    '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
                        borderColor: colorBorderGenre,
                    },
                    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                        borderColor: 'rgb(100,130,155)',
                    }
                }}
            >
            </TextField>
            <Checkbox onClick={toggle}/>
        </>
    }


    //=============================================> INPUT DEL "ARTISTA" <======================================

    //para desactivar el input con el checkbox;
    let [disable, setDisable] = React.useState(false);
    
    //actualizar el stilo del border del input 
    let [colorBorder, setColorBorder]=React.useState('rgb(0,30,55)');

    let [artistName,setArtist]=React.useState('');

    function handleArtisticName(event) {
        setArtist(event.target.value);
    }
    
    function inputArtistName() {
        
        function toggle(){
            setDisable(!disable);

            if ( colorBorder === 'rgb(0,30,55)' ) { 
                setColorBorder('rgb(70,130,125)') }
            else { 
                setColorBorder( 'rgb(0,30,55)' ) };
        }

        return <>
            <TextField onChange={handleArtisticName} label={'Nombre Artístico'}
                disabled={!disable}
                placeholder={userData.name || 'Vacío'}
                inputProps={{
                    style: {
                        color: 'rgb(130,180,185)', //color de la letra
                        backgroundColor: 'rgb(30,60,85)'
                    }
                }}
                InputLabelProps={{
                    style: { color: 'rgb(130,180,220)' }
                }}
                sx={{
                    '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
                        borderColor: colorBorder,
                    },
                    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                        borderColor: 'rgb(100,130,155)',
                    }
                }} >
            </TextField>
            <Checkbox onClick={ toggle } />
        </>
    }

    //=============================================> RESULTADO FINAL <======================================

    return (
        <div>
            <AppBar />
            <div style={{
                display: 'grid',
                placeContent: 'center',
                padding: '1rem'
            }}>

                <div style={container}>
                    <img src={imageCoverage || ''} alt="no image" className="coverImgProfile" />
                    <img key={updateImage} src="http://localhost:8000/api/profile/image" alt="profileImage"
                        style={{ borderRadius: '50%', width: '200px', height: '200px', border: '5px solid rgba(0,0,0,.5)', zIndex: 10 }} />
                    <div style={{marginTop:'5rem'}}>
                        {inputProfileImage()}
                        {inputCoverageImage()}
                    </div>
                    <div style={{
                        display: 'grid',
                        placeItems: 'left'
                    }}>
                        {userData !== null &&
                            <div style={{ margin: '1rem' }}>

                                {inputArtistName()}                               

                                {inputGenre()}
 
                                {inputDate()}

                                {inputBiography()}

                                <div style={{ display: 'grid', placeItems: 'center', margin: '1rem' }}>
                                    <Button onClick={sendData} variant="contained" style={{ backgroundColor: 'rgb(40,110,125)' }}>Guardar</Button>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}