import React, { useState } from 'react'
import AppBar from './appBar';
import { TextField, Button, Input, Checkbox } from '@mui/material';
import Swal from 'sweetalert2';


const Song = (song) => {
    let songData = song.song;

    const focusedColors = {
        '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgb(255,0,0)',
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgb(0,255,0)',
        }
    };

    const neonBox = {
        borderRadius: '.5rem',
        fontFamily: "monospace",
        backgroundColor: '#222831',
        border: '1px solid #77b3fe',
        color: '#77b3fe'
    };

    const neonBoxPadded = {
        borderRadius: '.5rem',
        fontFamily: "monospace",
        padding: '2rem 10rem 2rem 10rem',
        backgroundColor: '#222831',
        border: '1px solid #77b3fe',
        color: '#77b3fe'
    };

    let [artist, setArtist] = useState('')
    function changeArtist(event) {
        setArtist(event.target.value)
    }

    let [title, setTitle] = useState('');
    function changeTitle(event) {
        setTitle(event.target.value)
    }

    let [genre, setGenre] = useState('');
    function changeGenre(event) {
        setGenre(event.target.value);
    }

    let dataToUpdate: any = {};

    function getCSRFToken() {
        const csrf = document.querySelector('meta[name="csrf-token"]');
        if (csrf)
            return csrf.getAttribute('content');
        else return null;
    }

    function saveMeta() {
        console.log('se va a actualizar los datos siguientes');
        console.log(JSON.stringify({ fileId: songData.id, file: songData.file, artist, title, genre, image }))
        const CSRFToken = getCSRFToken();

        const formData = new FormData();
        formData.append('fileId', songData.id);
        formData.append('artist', artist);
        formData.append('file', songData.file);
        formData.append('title', title);
        formData.append('genre', genre);

        // Agregar la imagen al FormData
        if (image)
            formData.append('image', image);

        if (CSRFToken) {
            const init: RequestInit = { method: 'POST', headers: { 'X-CSRF-TOKEN': CSRFToken }, body: formData };

            fetch('http://localhost:8000/api/song/update', init).then((r) => {
                r.text().then((r) => {
                    console.log(JSON.parse(r));
                })
            })
        }
    }

    let [disableArtist, setDisableArtist] = React.useState(false);
    function changeBoxArtist() {
        setDisableArtist(!disableArtist)
    }

    let [disableTitle, setdisableTitle] = React.useState(false);
    function changeBoxTitle() {
        setdisableTitle(!disableTitle)
    }

    let [disableGenre, setdisableGenre] = React.useState(false);
    function changeBoxGenre() {
        setdisableGenre(!disableGenre)
    }

    let [image, setImage] = useState<File | null>(null);

    let [imageUrl, setImageUrl] = useState('');

    function handleNewImage(event) {
        setImage(event.target.files[0])
        imageUrl = URL.createObjectURL(event.target.files[0]);
        setImageUrl(imageUrl);


    }

    function handleTimeUpdate(event){
        const currentTime = event.target.currentTime;
        // Update the state with the current time
        console.log(currentTime);
    }

    function handleSeeked(event){
        console.log(event.target.currentTime);
    }

    function handleWheel(){
        }

    window.addEventListener('wheel', handleWheel, { passive: true });

    if (songData.metadata.metadata)
        return (
            <span >
                <AppBar />
                <span style={{ display: 'grid', placeItems: 'center', alignItems: 'center' }}>
                    <div style={{ backgroundColor: '#364252', border: '1px solid #77b3fe' }}>
                        <h1 style={{ color: '#77b3fe' }}>
                            <span style={neonBoxPadded}>Nombre del archivo: {songData.file}</span>
                        </h1>
                        <div style={{ padding: '1rem', display: 'grid', gridTemplateColumns: '50% 50%' }}>
                            <span style={{ color: '#77b3fe', margin: '.5rem', backgroundColor: '#222831', border: '1px solid #77b3fe', fontFamily: 'monospace', display: 'grid', placeItems: 'center' }}><h3>Agrega nuevos valores</h3></span>
                            <span style={{ color: '#77b3fe', margin: '.5rem', backgroundColor: '#222831', border: '1px solid #77b3fe', fontFamily: 'monospace', display: 'grid', placeItems: 'center' }}><h3>Valores actuales</h3></span>
                            {/**artista */}
                            <div>
                                <TextField variant='outlined'
                                    onChange={changeArtist}
                                    disabled={!disableArtist}
                                    label='Artista'
                                    style={{ margin: '.3rem' }}
                                    inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }} InputLabelProps={{ style: { color: '#77b3fe' } }} />
                                <Checkbox onChange={changeBoxArtist} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                            </div>
                            <div style={{ ...neonBox, ...{ fontSize: 'large', textAlign: 'center', display: 'flex', alignItems: 'center', placeItems: 'center', justifyContent: 'center' } }}>Artista: {songData.metadata.metadata.artist || 'Empty'}</div>

                            {/*titulo*/}
                            <div>
                                <TextField
                                    onChange={changeTitle}
                                    disabled={!disableTitle}
                                    variant='outlined'
                                    label='Título'
                                    style={{ margin: '.3rem' }}
                                    inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }}
                                    InputLabelProps={{ style: { color: '#77b3fe' } }} />
                                <Checkbox onChange={changeBoxTitle} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                            </div>
                            <div style={{ ...neonBox, ...{ fontSize: 'large', textAlign: 'center', display: 'flex', alignItems: 'center', placeItems: 'center', justifyContent: 'center' } }}>Título: {songData.metadata.metadata.title || 'Empty'}</div>

                            {/*genero */}
                            <div>
                                <TextField
                                    onChange={changeGenre}
                                    disabled={!disableGenre}
                                    variant='outlined'
                                    label='Género'
                                    style={{ margin: '.3rem' }} inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }} InputLabelProps={{ style: { color: '#77b3fe' } }} />
                                <Checkbox onChange={changeBoxGenre} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                            </div>
                            <div style={{ ...neonBox, ...{ fontSize: 'large', textAlign: 'center', display: 'flex', alignItems: 'center', placeItems: 'center', justifyContent: 'center' } }}>Género: {songData.metadata.metadata.genre || 'Empty'}</div>
                        </div>
                        <div style={{ display: 'grid', gridTemplateColumns: '50% 50%' }}>
                            <div style={{
                                border: '1px solid #77b3fe',
                                marginLeft: '1rem', backgroundColor: '#222831', display: 'grid', gridTemplateColumns: '50% 50%'
                            }}>
                                <div style={{
                                    marginLeft: '1rem',
                                    marginTop: '1rem',
                                    color: '#77b3fe',
                                    fontFamily: 'monospace',
                                    fontSize: 'large',
                                }}>
                                    Imagen
                                    <Input onChange={handleNewImage} type='file' style={{ margin: '1rem' }} />

                                </div>
                                {imageUrl && <img src={imageUrl} alt="Selected Image" style={{ minWidth: '100px', width: '200px', height: 'auto' }} />}


                            </div>
                            <audio 
                                id={songData.file}
                                src={"http://localhost:8000/api/song/" + songData.file} 
                                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                controls
                                onPlay={()=>{console.log('play')}}
                                onSeeked={()=>{console.log('seeked')}}
                                autoPlay={false}
                            ></audio>
                        </div>
                        <div style={{ display: 'grid', placeItems: 'center', margin: '1rem' }}>
                            <Button onClick={saveMeta} style={{ display: 'block', backgroundColor: 'rgb(60,150,150)' }} variant='contained'>Guardar</Button>
                        </div>

                    </div>
                </span>
                {/*<span style={{ padding: '1rem', backgroundColor: 'rgb(255,255,255)' }}>
                    <h3 >Metadados del archivo: {songData.fileName}</h3>
                        <TextField variant='standard' label='Título'>Título:{songData.title || 'Sin título'}</TextField>
                        Album:{songData.album || 'Sin album'}
                        Género:{songData.genre || 'Sin genero'}
    </span>*/}
            </span>);
    else {
        return (
            <div style={{
                padding: '1rem',
                display: 'grid', placeItems: 'center'
            }}>
                <div style={{backgroundColor:'rgb(80,80,100)', padding:'1rem'}}>
                    <span style={{
                        color: '#77b3fe',
                        margin: '.5rem',
                        backgroundColor: '#222831',
                        border: '1px solid #77b3fe',
                        fontFamily: 'monospace',
                        display: 'grid',
                        placeItems: 'center'
                    }}>
                        <h2 style={{padding:'1rem'}}>El archivo no tiene metadata</h2>
                    </span>
                        <h4>la metadata es necesaria para el correcto funcionamiento</h4>
                    {/**artista */}
                    <div>
                        <TextField variant='outlined'
                            onChange={changeArtist}
                            disabled={!disableArtist}
                            label='Artista'
                            style={{ margin: '.3rem' }}
                            inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }} InputLabelProps={{ style: { color: '#77b3fe' } }} />
                        <Checkbox onChange={changeBoxArtist} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                    </div>

                    {/*titulo*/}
                    <div>
                        <TextField
                            onChange={changeTitle}
                            disabled={!disableTitle}
                            variant='outlined'
                            label='Título'
                            style={{ margin: '.3rem' }}
                            inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }}
                            InputLabelProps={{ style: { color: '#77b3fe' } }} />
                        <Checkbox onChange={changeBoxTitle} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                    </div>

                    {/*genero */}
                    <div>
                        <TextField
                            onChange={changeGenre}
                            disabled={!disableGenre}
                            variant='outlined'
                            label='Género'
                            style={{ margin: '.3rem' }} inputProps={{ style: { color: '#77b3fe', backgroundColor: '#222831' } }} InputLabelProps={{ style: { color: '#77b3fe' } }} />
                        <Checkbox onChange={changeBoxGenre} inputProps={{ "aria-label": 'Habilitar' }} style={{ color: 'rgb(60,150,150)' }} />
                    </div>
                    <div style={{ display: 'grid', placeItems: 'center', margin: '1rem' }}>
                        <Button onClick={saveMeta} style={{ display: 'block', backgroundColor: 'rgb(60,150,150)' }} variant='contained'>Guardar</Button>
                    </div>
                </div>
            </div>)
    }

}

export default Song;