import * as React from 'react';
import Alert from '@mui/material/Alert';
import CheckIcon from '@mui/icons-material/Check';
import { CSSTransition } from 'react-transition-group';



interface SimpleAlertProps {
    message: string; // Define the message prop
    show:boolean; 
}

const SimpleAlert: React.FC<SimpleAlertProps> = ({ message, show }) => {

    return (
        <CSSTransition
        in={show}
        timeout={1000} // Duration of animation in milliseconds
        classNames="fade" // CSS class prefix for animations
        unmountOnExit // Remove component from DOM when not in transition
      >
        <Alert style={{margin:'1rem'}} icon={<CheckIcon fontSize="inherit" />} severity="success">
            {message}
        </Alert>
        </CSSTransition>
    );
}

export default SimpleAlert;