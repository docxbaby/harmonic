import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Badge from '@mui/material/Badge';
import MailIcon from '@mui/icons-material/Mail';
import {People} from '@mui/icons-material';
import { Navigate, useParams } from 'react-router-dom';
import PositionedMenu from './PositionedMenu';


export default function ButtonAppBar() {
  let [goProfile, setGoProfile] = React.useState(false);
  let [goScene, setGoScene] = React.useState(false);
  let [userName, setUserName] = React.useState('Mi otro nombre');

  let [goToNavigate, setGoToNavigate]=React.useState(false);

  function runSetGoToNavigate(){
      setGoToNavigate(true);
  }

  fetch('http://localhost:8000/api/user').then((e)=>{
    e.text().then((res)=>{
      const jres = JSON.parse(res);
      if(jres.userName){
        setUserName(jres.userName);
      }
    })
  })


  function navToProfile (){
    setGoProfile(true);
  }

  function goToScene(){
    setGoScene(true)
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" 
          style={{backgroundColor:'rgb(150,170,230)', color:'rgb(0,0,0)'}}>
        <Toolbar>
              <PositionedMenu/>
          <Typography onClick={goToScene} variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Button>Escena</Button>
          </Typography>
          <Badge badgeContent={4} color="secondary" style={{margin:'.3rem'}}>
            <MailIcon color="action" />
          </Badge>

          <Badge onClick={runSetGoToNavigate} badgeContent={4} color="secondary" style={{margin:'.3rem'}} >
            <People color="action" />
          </Badge>
          { goToNavigate && <Navigate to='/friend' />}

          {userName}
          <Button onClick={navToProfile} color="inherit">Perfil</Button>
          {goProfile && <Navigate to="/profile" replace={true} />}
          {goScene && <Navigate to="/scene" replace={true} />}
        </Toolbar>
      </AppBar> 
    </Box>
  );
}