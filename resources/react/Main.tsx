import React, { ReactNode } from 'react';
import { BrowserRouter, Routes, Route, Link, Navigate } from 'react-router-dom';
import Profile from './Profile';
import Dashboard from './Dashboard';
import Scene from './Scene';
import Song from './Song';
import Test from './test';
import FriendRequest from './FriendRequest';

const  Main = () => {
    let [editingSong, setEditingSong] = React.useState({ name: 'empty' });

    function setterEditingSong(song) {
        console.log('editando cancion', song)
        setEditingSong(song)
    }
 
    function handleWheel(){
    }
    window.addEventListener('wheel', handleWheel, { passive: true });

    return (
        <div>
            <div>
                <img style={{
                    backgroundImage: `url('images/Harmonic_blue.png')`,
                    backgroundRepeat: 'repeat',
                    width: '100vw',
                    height: '100vh',
                    position: 'fixed',
                    zIndex:'-10'
                }} />
                <img style={{
                    backgroundImage: `url('images/Harmonic_pink.png')`,
                    backgroundRepeat: 'repeat',
                    width: '100vw',
                    height: '100vh',
                    position: 'fixed',
                    zIndex:'-9'
                }} />
            </div>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Dashboard setEditingSong={setterEditingSong}/>} />
                    <Route path="/profile" element={<Profile/>} />
                    <Route path="/dashboard" element={<Dashboard setEditingSong={setterEditingSong}/>} />
                    <Route path="/scene" element={<Scene/>} />
                    <Route path="/song" element={<Song song={editingSong}/>} />
                    <Route path="/test" element={<Test/>} />
                    <Route path="/friend" element={<FriendRequest/>} />
                </Routes>
            </BrowserRouter>
            <div style={{ display:'flex', justifyContent:'center', bottom:0, backgroundColor:'rgb(120,70,130)', color:'white', width:'100%'}}>Harmonic 2027 Autor: Juan Pablo Hernández Guzman</div>
        </div >
    );
}

export default Main;