<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;

use getid3; 
use getid3_writetags;

class SongsController extends Controller {

    /**almacena el archivo de la canción */
    public function add(Request $request){
        
        $getID3 = new \getID3;

        if (!$request->hasFile('file'))
        return ['status'=>'error', 'message'=>'Petición incorrecta:Falta el archivo']; 

        // Store the uploaded file in the storage directory
        $file = $request->file('file');
        $fileInfo = $getID3->analyze($file);
        
        $error = $fileInfo['error'] ?? null;
        if ($error)
        return ['status'=>'error', 'message'=>'No se pudo leer el archivo almacenado'. $fileName]; 
    
    
        //extraemos la metadata 
        $title = $fileInfo['tags']['id3v2']['title'][0] ?? null;
        $artist = $fileInfo['tags']['id3v2']['artist'][0] ?? null;
        $genre = $fileInfo['tags']['id3v2']['genre'][0] ?? null;
        
        //insertamos relacion usuario y archivo 
        $fileName = $file->getClientOriginalName(); // Use a unique name if needed
        $userId = session('userId');
        $fileName = $userId .'_'. $fileName; 

        $fileId = Self::insertFileUser($fileName);

        if (gettype($fileId) != 'integer' ){
            return ['status'=>'error', 'message'=>'No se pudo insertar el archivo', 'motivo' => $fileId['message']]; 
        }

        if ( !Self::insertMeta($title,$artist,$genre,$fileId) ) {
            DB::table('songs')->where('id', $fileId)->delete();
            return ['status'=>'error', 'message'=>'No contiene metadata']; 
        }
        
        $file->storeAs('uploads/songs/', $fileName); // Store the file in the 'uploads' directory
        $path = dirname(dirname(dirname(__Dir__))). '\\storage\\app\\uploads\\songs\\';
        $filePath = $path . $fileName;
        
        return ['status'=>'success', 'message'=> 'Cancion subida']; 
    }

    function insertFileUser($fileName){
        try{

            $userId = session('userId');
            $current_time = date("Y-m-d H:i:s");

            return $fileId = DB::table('songs')->insertGetId(['file'=>$fileName,
            'created_at'=> $current_time, 
            'updated_at'=> $current_time,
            'user_Id' => $userId ]);
        }catch(QueryException $e){
            return ['status' => 'error', 'message' => 'el archivo ya ha sido subido' ];
        }
    }

    static function insertMeta($title, $artist, $genre, $fileId){
        
        Log::debug('title:' . $title); 
        Log::debug('artist:'. $artist); 
        Log::debug('genre:' . $genre); 
       
        Log::debug('title:'  . $title); 
        Log::debug('artist:' . !$artist); 
        Log::debug('genre:' . !$genre); 

        if( !$title || !$artist || !$genre ){
            return false;
        }

        $current_time = date("Y-m-d H:i:s");
        try {
            return DB::table('song_meta_data')->insert( 
                ['title' => $title, 
                'artist' => $artist, 
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'genre' => $genre,  
                'fileId' => $fileId ] );
                
            } catch (\QueryException $e) {
                return false;
            }

            return false;
    }

    function splitRoute($path){
        return explode('/', $path)[2];
    }

    function update(Request $request){
        $getID3 = new \getID3;

        $formData = $request->all();
        
        $userId = session('userId');
        $path = dirname(dirname(dirname(__Dir__))). '\\storage\\app\\uploads\\songs\\';
        $songFileName = $formData['file'];
        $fullSongPath = $path . $songFileName;
        $fileInfo = $getID3->analyze($fullSongPath);
        $tagWriter = new getid3_writetags;

        $error = $fileInfo['error'] ?? null;
        if ($error){
            return ['status'=> 'success', 'message' => 'no se pudo leer el archivo' .  $fullSongPath];
        }
        else {

            if(!isset($fileInfo['tags'])){
                $tagWriter->filename = $fullSongPath;
                $tagWriter->tag_data = 
                    //['tags' => [ 'id3v2' => [ 'artist' => '', 'genre' => '', 'title' => '' ] ]];
                    ['tags' => [ 'id3v2' => [] ]];
              
                if ($tagWriter->WriteTags()){
                    $fileInfo = $getID3->analyze($fullSongPath);

                    return [ 'status' => 'success', 'warnings' => $tagWriter->warnings,'errors' => $tagWriter->errors, 'info' => $fileInfo];

                    $tags = $fileInfo->tags;

                    $artist = $formData['artist'];
                    if($artist)
                        $tags['id3v2']['artist'] = $artist; 
        
                    $title = $formData['title'];    
                    if($title)
                        $tags['id3v2']['title'] = $title;
        
                    $genre = $formData['genre'];
                    if($genre)
                        $tags['id3v2']['genre'] = $genre;
        
                    $fileInfo['tags'] = $tags; 
        
                    $taggingFormat = 'UTF-8';
                    
                    $tagWriter->filename       = $fullSongPath;
                    $tagWriter->tagformats = array('id3v2.3');
                    $tagWriter->tag_encoding   = $taggingFormat;
        
                    $tagWriter->tag_data = $tags;

                }
                else {
                    return ['status'=>'success', 
                            'message' => 'Se intento crearle metadatos sin exito', 
                            'file' => $fullSongPath,
                            'writerFileName' => $tagWriter->filename,
                            'warnings' => $tagWriter->warnings,
                            'errors' => $tagWriter->errors];
                }
            }
            if (!$tagWriter->WriteTags()){
                return ['status' => 'error', 'message' =>'no se pudieron escribir los metadatos' ];
            } 

        }
    
        $image = $request->file('image');
        if ($image){
            $imgOriginalName = $image->getClientOriginalName(); // Use a unique name if needed
            $imageFileName = $songFileName . '_' . $imgOriginalName;
            $image->storeAs('uploads/images/songs', $imageFileName);
            }

        return ['status'=>'success', 'message' =>'Datos actualizados', 'file' => $fullSongPath, 'warnings'=> $tagWriter->warnings ];
    }
    

    /**
     * devuelve los registros de las canciones de un usuario
     */
    function getSongs(){
        $userId = session('userId');
        if(!$userId)
        return ['status'=> 'error', 'message' => 'No se ha logeado'];
        
        $records = DB::table('songs')->where('user_id', '=', $userId)->get();
        return self::combine($records);
    }

    /**
     * busca los registros de las canciones 
     * y por cada uno busca su archivo con sus metadatos 
     */
    private function combine($records){
        $files = self::songsByUserId();

        foreach ($records as $record) {
            foreach($files['songs'] as $file){
                if( $record->file === $file['fileName'] ) {
                    $record->metadata = $file; 
                }
            }
        }
        return $records; 
    }

    /**
     * busca los archivos de canciones de un usuario
     */
    public function songsByUserId()
    {
        $files = Storage::files('uploads/songs');
        $userId = session('userId');
        $prefix = $userId . '_';

        $getID3 = new \getID3;

        //los archivos solo del usuario 
        $matchingFiles = array_filter($files, function ($file) use ($prefix) {
            return strpos(basename($file), $prefix) === 0;
        });

        //obtenemos el puro nombre del archivo sin la ruta relativa 
        $splited = array_map([self::class, 'splitRoute'], $matchingFiles);

        //obtenemos la metadata de cada archivo
        $filesWithMetaData = array();

        foreach ($splited as $file){
            $path = dirname(dirname(dirname(__Dir__))). '\\storage\\app\\uploads\\songs\\';
            $fullPath = $path . $file; //ruta absoluta 
            $fileInfo = $getID3->analyze($fullPath);
            $error = $fileInfo['error'] ?? null;

            if (!$error){
                $metadata = $fileInfo['tags']['id3v2'] ?? null;
                $filesWithMetaData = [...$filesWithMetaData, ['fileName' => $file, 'metadata' => $metadata]];
            }
            else {
                array_push($filesWithMetaData, ['fileName' => $file, 'metadata' => 'error']); 
                Log::debug('No se pudo leer el archivo' . $fullPath);
            }
        }
        
        return ['status' =>'sucess', 'message' => 'Caciones encontradas', 'songs' => $filesWithMetaData];
    }

    public function songByName($songName){
        $userId = session('userId');
        $filePath = 'uploads/songs/' . $songName;

        if (Storage::exists($filePath)) {
            Log::error($filePath);          
            return Storage::response($filePath);
        } else {
        // Return an error response if the file does not exist
            return response()->json(['error' => 'File not found:' . $filePath], 404);
        }
    }

    /**
     * para actualizar los meta datos de una canción
     */
    public function getMeta(Request $request){
        $getID3 = new \getID3;
        $tagWriter = new getid3_writetags;
        $TaggingFormat = 'UTF-8';
        $getID3->setOption(array('encoding'=>$TaggingFormat));


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileInfo = $getID3->analyze($file);
            $tagWriter->tag_data = ['artist' => 'Snoop Dog'];
            $path = dirname(dirname(dirname(__Dir__))). '\\storage\\app\\uploads\\songs\\';
            $filename = 'snoopFile.mp3';
            $file->storeAs('uploads/songs/', $filename);
            $tagWriter->filename = $path.'snoopFile.mp3';
            $tagWriter->remove_other_tags = true;
            $tagWriter->overwrite_tags = true   ;

            if ($tagWriter->WriteTags()){
                $fileInfo = $getID3->analyze($file);
                return $fileInfo;
            }
            else {return ['errors' => $tagWriter->errors];}

        }
    }

    
}