<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;




class UsuarioController extends Controller
{
  function saveUser(Request $request){
    try {
        $formData = $request->all();
        $email = $formData['email'];
        $username = $formData['username'];
        $password = $formData['password'];
        $hashedPassword = Hash::make($password);
        $now = Carbon::now()->timezone('America/Mexico_City');

        DB::table('usuarios')->insert([
            'email' => $email,
            'username' => $username,
            'password' => $hashedPassword,
            'created_at' => $now, 
            'updated_at' => $now
        ]);
        return response()->json(['message' => 'User saved successfully'], 200);

    } catch (\Exception $e) {
        Log::error($e->getMessage());
        return response()->json($e);
    }
  }

  function logout(){
    Session::forget('userId');
    return ['status' => 'success', 'message' => 'session cerrada' ];
  }

  /**valida si ya se ha loggeado */
  function logged(){
    Log::debug('validando logged');
    Log::debug(session()->has('userId'));
    return ['status'=> 'success', 'logged' => session()->has('userId')] ;
  }


  function getUser(){
    return ['userName' => session('userName')];
  }

  function getUserData(){

    $userId = session('userId'); 
    $userData = DB::table('usuarios')->where('id','=', $userId)->get()[0];

    if ($userData) {
      // Devolver los datos del usuario si existe
      return ['status' => 'success', 'userData' => $userData];
  } else {
      // Devolver un mensaje de error si el usuario no existe
      return ['status' => 'error', 'message' => 'El usuario no existe'];
  }
  }

  function login(Request $request){
    $formData = $request->all();
    $username = $formData['username'];
    $password = $formData['password'];
    $user  = DB::table('usuarios')->where('username' , $username)->first();
    $count = DB::table('usuarios')->where('username' , $username)->count();

    if ($count == 0){
        return json_encode(['status'=>'error', 'message' => 'No se encontró el usuario']);
    }
    
    //Log::error($user);
    session(['userId'=>$user->id]); 
    session(['userName'=>$user->username]); 
    session(['name'=>$user->name]); 
    $hashedPass = $user->password; 

    if ( Hash::check($password,$hashedPass) ){
        return json_encode(['status'=>'success', 'message' => 'Usuario encontrado']);
    }else {
        return json_encode(['status'=>'error', 'message' => 'No se encontró el usuario']);

    }

  }
}
