<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;


class ProfileController extends Controller {

    public function getImage(){
        $userId = session('userId');
        $dir='uploads/images/';
        $files = Storage::files($dir);
        $filePath = $userId . '_profileImage';

        $matchingFiles = array_filter($files, function ($file) use ($filePath) {
            return strpos(basename($file), $filePath) === 0;
        });

        
        if(count($matchingFiles) === 0)
        return ['status' => 'error', 'message' => 'no images'];

        if ( Storage::exists($matchingFiles[0]) ) {
            return Storage::response($matchingFiles[0]);
        }
        else {
            return Storage::response('uploads/images/noProfileImage.jpg');
            //return ['no se encontro' . $filePath];
        }
    }

    /**
     * imagen de portada
     */
    public function upCoverage(Request $request){
        $file = $request->hasFile('coverage');
        if ($file) {
            $file = $request->file('coverage');
            $userId = session('userId');
            $originalName = $file->getClientOriginalName(); 
            $ext = substr($originalName, mb_strlen($originalName) -3 , mb_strlen($originalName)); 

           if ( $file->storeAs('uploads/images/coverage/', $userId . '_coverage'. $ext  ) ) 
                return ['status' => 'success', 'message' => 'se almaceno la imagen']; 
            else
            return ['status' => 'error', 'message' => 'Error al almacenar la imagen']; 
        }
        else 
          return ['status' => 'error', 'message' => 'Error al recibir el archivo']; 
    }

    public function getCoverage(){
        $userId = session('userId');

        if (!$userId)
        return ['status' => 'error', 'message' => 'User not logged'];

        $dir='uploads/images/coverage/';
        $files = Storage::files($dir);
        $filePattern = $userId . '_coverage';

        
        $matchingFiles = array_filter($files, function ($file) use ($filePattern) {
            return strpos(basename($file), $filePattern) === 0;
        });

        if(count($matchingFiles) === 0)
            return Storage::response('uploads/images/coverage/coverage.jpg');
        else 
            //return Storage::response($matchingFiles[1]);
        foreach($matchingFiles as $file){
            return Storage::response($file);
        }
    }

    public function saveProfileData( Request $request){
        $formData = $request->all();
        
        $date = $formData['fundationDate'];
        $artistName = $formData['artistName'];
        $userId = session('userId');
        $genre = $formData['genre'];
        $bio = $formData['biography'];
        
        $fieldsToUpdate = [];
        
        if($date)
        $fieldsToUpdate = ['fundation' => $date];

        if($artistName)
        $fieldsToUpdate = [ ...$fieldsToUpdate,  'name' => $artistName  ];

        if($genre)
        $fieldsToUpdate = [ ...$fieldsToUpdate,  'genre' => $genre  ];

        if($bio)
        $fieldsToUpdate = [ ...$fieldsToUpdate,  'biography' => $bio  ];

        $result = DB::table('usuarios')
        ->where('id', $userId) // Assuming $userId contains the ID of the record you want to update
        ->update($fieldsToUpdate);
            
        if ($result)
            return response()->json(['status' => 'success', 'message' => 'se actualizo' ]);
        else
            return response()->json(['status' => 'error', 'message' => 'No se pudo actualizar' ]);
    }

    public function saveImage(Request $request){
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originalName = $file->getClientOriginalName(); // Use a unique name if needed
            $fileName = 'profileImage';
            $userId = session('userId');
            $fileName = $userId .'_'. $fileName; 
            $ext = substr($originalName, mb_strlen($originalName) -3 , mb_strlen($originalName)); 
            $file->storeAs('uploads/images/', $fileName . '.'. $ext); 

        return ['status'=>'success', 'message'=>'archivo almacenado:'. $ext ]; 
        }
        else {
            return ['status'=>'error', 'message'=>'Petición incorrecta:Falta el archivo']; 
        }        
    }
}