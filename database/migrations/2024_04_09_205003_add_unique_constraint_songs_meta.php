<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('songs', function (Blueprint $table) {
            // Add a unique constraint to the specified column(s)
            $table->unique('file');
        });
    }

    public function down()
    {
        Schema::table('songs', function (Blueprint $table) {
            // Drop the unique constraint if needed
            $table->dropUnique('file');
        });
    }
};
