<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('song_meta_data', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('fileId')->constrained('songs');
            $table->string('author');
            $table->string('name');
            $table->string('genre');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('song_meta_data');
    }
};
