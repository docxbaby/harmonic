<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->string('genre')->nullable();
            $table->string('biography')->nullable();
            $table->date('fundation')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->dropColumn('genre'); // Rollback: Remove the added field
            $table->dropColumn('biography'); // Rollback: Remove the added field
            $table->dropColumn('fundation'); // Rollback: Remove the added field
        });
    }
};
