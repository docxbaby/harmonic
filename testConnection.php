<?php
$serverName = "localhost\MSSQLSERVER01";
$connectionOptions = array(
    "Database" => "harmonic",
);
// Intenta conectar
$conn = sqlsrv_connect($serverName, $connectionOptions);
if ($conn === false) {
    // Error al conectar
    echo "Error al conectar a la base de datos: ";

    
    foreach (sqlsrv_errors() as $value) {
        foreach($value as $a){
            echo $a .'\n'; 
        }
    }

} else {
    // Conexión exitosa
    echo "Conexión exitosa a la base de datos.";
    // Cierra la conexión
    sqlsrv_close($conn);
}
?>






