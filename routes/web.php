<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use app\Controllers\UsuarioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Define a fallback route
Route::fallback(function () {
    return redirect('/');
});

Route::post('/api/save-user','App\Http\Controllers\UsuarioController@saveUser' );
Route::get('/api/user', 'App\Http\Controllers\UsuarioController@getUser');
Route::get('/api/userData', 'App\Http\Controllers\UsuarioController@getUserData');

Route::post('/api/login','App\Http\Controllers\UsuarioController@login' );
Route::get('/api/logout','App\Http\Controllers\UsuarioController@logout' );
Route::get('/logged/','App\Http\Controllers\UsuarioController@logged' );
Route::post('/api/songs', 'App\Http\Controllers\SongsController@add');
Route::get('/api/songs', 'App\Http\Controllers\SongsController@songsByUserId');
Route::get('/api/song/{songName}', 'App\Http\Controllers\SongsController@songByName');
Route::get('/api/songs_v2/', 'App\Http\Controllers\SongsController@getSongs');
Route::post('/api/song/update', 'App\Http\Controllers\SongsController@update');
Route::post('/api/musicFile/getMeta', 'App\Http\Controllers\SongsController@getMeta');

Route::get('/api/profile/image', 'App\Http\Controllers\ProfileController@getImage');
Route::post('/api/profile/image', 'App\Http\Controllers\ProfileController@saveImage');
Route::post('/api/profile/coverage', 'App\Http\Controllers\ProfileController@upCoverage');
Route::get('/api/profile/coverage', 'App\Http\Controllers\ProfileController@getCoverage');
Route::post('/api/profile/saveProfile', 'App\Http\Controllers\ProfileController@saveProfileData');
